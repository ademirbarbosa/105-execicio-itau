package tech.mastertech.itau.jogo21;

import java.util.ArrayList;
import java.util.List;

public class Baralho {

	public static List<Carta> gerarBaralho() {
		List<Carta> baralho = new ArrayList<>();

		for (TipoDeCarta tipo : TipoDeCarta.values()) {
			for (NaipeDaCarta naipe : NaipeDaCarta.values()) {
				Carta carta = new Carta();
				carta.setNaipe(naipe);
				carta.setNome(tipo);
				carta.setValor(tipo.getPontuacao());
				baralho.add(carta);

			}
		}
		return baralho;
	}
}
