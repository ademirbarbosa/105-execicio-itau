package tech.mastertech.itau.jogo21;

import java.util.List;

public class Jogo {
	private int pontuacao;
	private int rodada = 0;
	
	public void executarJogo() {
		List<Carta> baralho = Baralho.gerarBaralho();
		if(rodada == 0) {
			executarRodada(baralho);
			executarRodada(baralho);
			rodada++;
		}
			String resposta = Interface.perguntar();
			if (resposta.toLowerCase().equals("s")) {
				executarRodada(baralho);
				rodada++;
			}
			else {
				encerrarJogo();
			}
		
			if(pontuacao < 21) {
				executarJogo();
			}else {
				encerrarJogo();
			}
	
	}
	
	public void executarRodada(List<Carta> baralho) {
		Sorteador sorteador = new Sorteador();
		Carta carta = sorteador.darCarta(baralho);
		marcarPontucao(carta.getValor());
		Interface.mostrarCarta(carta);
		Interface.mostrarPontuacao(pontuacao);
	}
	
	public void marcarPontucao(int valor) {
		this.pontuacao += valor;
	}
	
	public void encerrarJogo() {
		System.exit(0);
	}
}
