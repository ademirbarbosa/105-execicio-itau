package tech.mastertech.itau.jogo21;

import java.util.List;

public class Sorteador {
	
	public Carta darCarta(List<Carta> baralho) {
		int sorteio = (int)(Math.floor(Math.random()*baralho.size()));
		Carta carta = baralho.get(sorteio);
		baralho.remove(sorteio);
		return carta;
		
	}
}
