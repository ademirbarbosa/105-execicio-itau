package tech.mastertech.itau.jogo21;

import java.util.Scanner;

public class Interface {

	public static String perguntar() {
		Scanner sc = new Scanner(System.in);
		String pergunta = "Quer mais uma carta? s/n";
		System.out.println(pergunta);
		String resposta = sc.nextLine();

		if (!resposta.equals("s") && !resposta.equals("n")) {
			System.out.println("Resposta Invalida");
			perguntar();
		}
		return resposta;
	}
	
	public static void mostrarCarta(Carta carta) {
		System.out.println(carta.getNome() + " " + carta.getValor() + " " + carta.getNaipe());
	}
	
	public static void mostrarPontuacao(int p) {
		System.out.println("Pontuação: "+p);
	}
}
