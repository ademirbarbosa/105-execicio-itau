package tech.mastertech.itau.jogo21;

import java.util.ArrayList;
import java.util.List;

public class Carta {
	private TipoDeCarta nome;
	private int valor;
	private NaipeDaCarta naipe;
	
	public TipoDeCarta getNome() {
		return nome;
	}
	public void setNome(TipoDeCarta nome) {
		this.nome = nome;
	}
	public int getValor() {
		return valor;
	}
	public void setValor(int valor) {
		this.valor = valor;
	}
	public NaipeDaCarta getNaipe() {
		return naipe;
	}
	public void setNaipe(NaipeDaCarta naipe) {
		this.naipe = naipe;
	}
	

	
}
